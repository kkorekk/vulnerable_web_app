from django.conf.urls import url

from .views import (
    DeleteImageView,
    ImagesListView,
    SignUpView,
    SiteConfigurationView,
    UploadImageView,
)

urlpatterns = [
    url(r'^$', ImagesListView.as_view(), name='images-list'),
    url(r'^signup/$', SignUpView.as_view(), name='sign-up'),
    url(r'^upload-image/$', UploadImageView.as_view(), name='upload-image'),
    url(
        r'^delete-image/(?P<image_id>[0-9]+)',
        DeleteImageView.as_view(),
        name='delete-image',
    ),
    url('^site-conf/$', SiteConfigurationView.as_view(), name='site-config'),
]
