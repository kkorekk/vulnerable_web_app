from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from .models import Image, SiteConfig


User = get_user_model()


class ImageModelForm(ModelForm):

    class Meta:
        model = Image
        fields = ('image', 'title', 'description')


class SiteConfigForm(ModelForm):

    def __init__(self, *args, **kwargs):
        instance = SiteConfig.objects.first()

        if instance is None:
            instance = SiteConfig.objects.create(name='Default Site Name')

        super().__init__(instance=instance, *args, **kwargs)

    class Meta:
        model = SiteConfig
        fields = '__all__'


class SignUpForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        fields = ('username', 'first_name', 'last_name', 'email')
