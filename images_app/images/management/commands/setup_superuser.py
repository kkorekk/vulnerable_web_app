from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


User = get_user_model()


class Command(BaseCommand):
    help = 'Sets up super user'

    def handle(self, *args, **options):
        user, created = User.objects.get_or_create(
            username='admin',
            is_staff=True
        )

        if created:
            user.set_password('adminadmin')
            user.save()
            self.stdout.write('User admin created')
        else:
            self.stdout.write('User admin already exists')
