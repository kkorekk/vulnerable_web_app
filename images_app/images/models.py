from django.conf import settings
from django.db import models


class Image(models.Model):
    image = models.FileField(upload_to='images')
    title = models.CharField(max_length=250)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta:
        ordering = ('-created', )


class SiteConfig(models.Model):
    name = models.CharField(max_length=100)
