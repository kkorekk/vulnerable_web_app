from .models import SiteConfig


def site_name(request):
    site_config = SiteConfig.objects.first()
    return {
        'site_name': site_config.name if site_config else 'Default Site Name'
    }