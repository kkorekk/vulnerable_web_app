from django.contrib.auth import login
from django.http import (
    HttpResponse, HttpResponseRedirect, HttpResponseNotFound,
)
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.views.generic.list import ListView

from .forms import ImageModelForm, SignUpForm, SiteConfigForm
from .models import Image, SiteConfig


class DeleteImageView(View):

    def delete(self, request, image_id):
        if not request.user.is_authenticated():
            return HttpResponseNotFound()

        image = get_object_or_404(Image, pk=image_id)
        image.delete()

        return HttpResponse(status=200)


class ImagesListView(ListView):

    model = Image
    template_name = 'image_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['upload_form'] = ImageModelForm()
        return context


class SignUpView(FormView):
    form_class = SignUpForm
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('images-list')


class SiteConfigurationView(FormView):
    form_class = SiteConfigForm
    template_name = 'site_configuration.html'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('images-list')


class UploadImageView(View):

    @staticmethod
    def get_success_url():
        return reverse('images-list')

    def post(self, request):
        if not request.user.is_authenticated():
            return HttpResponseNotFound()

        form = ImageModelForm(request.POST, request.FILES)

        if form.is_valid():
            image = form.save(commit=False)
            image.owner = request.user
            image.save()

        return HttpResponseRedirect(redirect_to=self.get_success_url())
